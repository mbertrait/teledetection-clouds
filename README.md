# Cloud detection in optical satellite images with a modified k-means

This project is a C implementation of Laurent Baudoin et al. paper
[Introducing spatial information in k-means algorithm for clouds detection in optical satellite images](http://adsabs.harvard.edu/abs/2001SPIE.4168...67B).

A copy of the pdf of the paper above is included in the repo (europto_2000.pdf)

Sections:

1. [The algorithm and its utility](#algorithm)
2. [Results](#results)
3. [Compilation and Execution](#compilation-and-execution)

## The algorithm and its utility

Among all the satellites around the earth, some of them take pictures of the ground
in order to transfer them to analysts on ground. The time to send a picture is quite
long and needs multiple upload of it, since satellites move quickly, the satellite is
able again to continue the transfer by finding another reciever antenna.

The problem is that if the picture taken was of a cloudy area, the transfer will
be useless since the picture will be unexploitable. The solution is to make
the satellite decide wether it is worth it to send the picture taken on earth or
not based on the % of clouds detected on the picture.

This task is done by the algorithm described in the paper and implemented in this
project.

## Results

![Example 1](EXAMPLES/1.png)
Percentage of clouds: 0.42 %

![Example 2](EXAMPLES/2.png)
Percentage of clouds: 1.52 %

## Compilation and Execution

```console
root@:~$ cd SRC
root@:~/SRC$ make
```
The compilation creates two directories: EXE and OBJ.  
* EXE will contains the two binaries (imaProjet.exe and imaProjet_auto.exe)  
* OBJ will contains all object files created with compilation

The imaProjet.exe binary launch a GUI interface where you can see the result of
clouds detection after imported the desired image throw the GUI.
```console
root@:~/SRC$ cd ../EXE
root@:~/EXE$ ./imaProjet.exe
```

The imaProjet_auto.exe binary launch the clouds detection with percentage print
on all the satellite images contained in [IMAGES](IMAGES/) folder (.bmp files only)
```console
root@:~/SRC$ cd ../EXE
root@:~/EXE$ ./imaProjet_auto.exe
```
