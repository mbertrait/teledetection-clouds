#include <stdio.h>
#include <gtk/gtk.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>

#include "utils.h"
#include "data.h"

/*  @Author Maxime BERTRAIT
 *
 *  I choosed to follow the europto_200O report and to consider
 *  one mass_center as the clouds one. I choosed also 8/9 classes.
 *  As explained in the report, choosing 8/9 classes is the best compromise.
 *
 *  If we want best results that fit much more the clouds on the given images,
 *  we can take pixels classified as clouds (close to mass_center of clouds
 *  indexed by nb_classes - 1) and the pixels classified just one class before
 *  (mass_center indexed by nb_classes - 2), but we take a risk to
 *  have false-positive and we really don't want to have false-positive.
 *
 *  So the results you will obtain by this algorithm will not be as high as you
 *  may obtain with less classes or by assuming that clouds are in the two last
 *  cluster and not only the last, but they will be the more relevant of whether
 *  the pixel is part of a cloud or not in terms of true-positive.
 *
 */

void ComputeImage(guchar *pucImaOrig,
    int NbLine,
    int NbCol,
    guchar *pucImaRes)
{
  unsigned int iNbPixelsTotal;
  unsigned int iNbChannels=3; /* We're working with RGB images */
  iNbPixelsTotal=NbCol*NbLine;

  int ch = 1; // It's better to choose Green channel
  bool is_panchrom = true;

  /* Check wether it is a panchromatique image or not */
  for (unsigned int s = 0; s < iNbPixelsTotal; s++) {
    if (!(*(pucImaOrig+(s*iNbChannels)) == *(pucImaOrig+(s*iNbChannels)+1)
          && *(pucImaOrig+(s*iNbChannels)) == *(pucImaOrig+(s*iNbChannels)+2))) {
      is_panchrom = false;
      break;
    }
  }

  int nb_classes = 8; /* as recommended in the europa2000 report */
  if (is_panchrom)
    nb_classes = 9; /* as recommended in the europa2000 report */

  /* Allocation of all the structures (arrays ect.) */

  /* nb_px will contains the number of clouds per classes (cluster) */
  unsigned int *nb_px = calloc(nb_classes, sizeof(unsigned int));

  /* cl_px will contains the pixels and their actual assigned
   * class (see struct data) */
  struct data *cl_px = malloc(sizeof(struct data) * iNbPixelsTotal);

  /* mass_centers will represent each mass_center as 5 components
   * (one mass_center per class) */
  unsigned int** mass_centers = malloc(sizeof(unsigned int*)*nb_classes);

  /* newMCs will be, at each end of iteration, the next mass_centers.
   * It will be usefull to see changes between last iteration and current */
  unsigned int **newMCs = malloc(sizeof(unsigned int*) * nb_classes);

  /* Make sure that mallocs didn't failed */
  if (cl_px != NULL && nb_px != NULL && cl_px != NULL
      && mass_centers != NULL && newMCs != NULL) {
    
    /* Initialisation of all 4-connexity vectors */
    struct data *iterator = cl_px;
    for (unsigned int s = 0; s < iNbPixelsTotal; s++) {
      unsigned int* Vpixel = malloc(sizeof(unsigned int) * 5);
      Vpixel[0] = *(pucImaOrig + (s*iNbChannels) + ch);
      Vpixel[1] = s/NbCol >= 1 ? *(pucImaOrig + ((s*iNbChannels) - (NbCol*3)) + ch) : 255;
      Vpixel[2] = s%NbCol == 0 ? 255 : *(pucImaOrig + ((s*iNbChannels) - 3) + ch);
      Vpixel[3] = (s+1)%NbCol == 0 ? 255 : *(pucImaOrig + ((s*iNbChannels)) + iNbChannels + ch);
      Vpixel[4] = s/NbCol < NbLine - 1 ? *(pucImaOrig + ((s*iNbChannels) + (NbCol*iNbChannels)) + ch) : 255;
      qsort(Vpixel, 5, sizeof(unsigned int), compare); // We sort the 4-connexity vector
      iterator->px = Vpixel;
      iterator->cluster = 0;
      iterator = iterator + 1;
    }

    /* Initialization of the mass centers */
    for (int k = nb_classes - 1; k >= 0; k--) {
      mass_centers[k] = malloc(sizeof(unsigned int)*5);
      mass_centers[k][0] = (255*k)/(nb_classes-1);
      mass_centers[k][1] = (255*k)/(nb_classes-1);
      mass_centers[k][2] = (255*k)/(nb_classes-1);
      mass_centers[k][3] = (255*k)/(nb_classes-1);
      mass_centers[k][4] = (255*k)/(nb_classes-1);
    }

    /* Initialisation of future mass_centers */
    for (unsigned int k = 0; k < nb_classes; k++)
      for (unsigned int i = 0; i < 5; i++)
        newMCs[k] = calloc(5, sizeof(unsigned int));

    bool changes = false;
    
    /* While there is modification of centers */
    do {
      
      for (unsigned int i = 0; i < nb_classes; i++)
        nb_px[i] = 0;

      changes = false;
      struct data *ite = cl_px;

      /* Classify each Vpixel */
      for (unsigned int s = 0; s < iNbPixelsTotal; s++) {
        int min_cl = 0;
        float min = FLT_MAX;
        for (unsigned int k = 0; k < nb_classes; k++) {
          float cp_dist = dist_manht(ite->px, mass_centers[k]);
          if (cp_dist < min) {
            min = cp_dist;
            min_cl = k;
          }
        }
        ite->cluster = min_cl;
        nb_px[min_cl] += 1;
        ite = ite + 1;
      }

      for (unsigned int i = 0; i < nb_classes; i++)
        for (unsigned int j = 0; j < 5; j++)
          newMCs[i][j] = 0;

      struct data *it = cl_px;
      for (unsigned int s = 0; s < iNbPixelsTotal; s++) {
        unsigned int *pixel = it->px;
        for (unsigned int v = 0; v < 5; v++)
          newMCs[it->cluster][v] += pixel[v];
        it = it + 1;
      }
      
      /* Recompute mass_centers */
      for (unsigned int c = 0; c < nb_classes; c++) {
        if (nb_px[c] > 0) {
          for (unsigned int i = 0; i < 5; i++) {
            newMCs[c][i] /= nb_px[c];
            if (!changes && (c < nb_classes - 1) && mass_centers[c][i] != newMCs[c][i])
              changes = true;
            if (c < nb_classes - 1)
              mass_centers[c][i] = newMCs[c][i];
          }
        }
      }

      /* Here there is no need to qsort as the Vpixels are already sorted.
         As so, we can directly take the middle value as the median. */

      /* Special treatment for mass_center of clouds  as it has to remain on homogeneity axis */
      if (nb_px[nb_classes - 1] > 0) {
        unsigned int median = newMCs[nb_classes - 1][2];
        for (unsigned int i = 0; i < 5; i++) {
          if (!changes && median != mass_centers[nb_classes - 1][i])
            changes = true;
          mass_centers[nb_classes - 1][i] = median;
        }
      }

    } while (changes); // While there is almost one change on mass_centers
  }

  struct data *iter = cl_px;
  for(int d = 0; d < iNbPixelsTotal; d++) {
    /*
     *  If you want better fiting on image you can
     *  add the pixels classified in the nb_classes - 2
     *  as I said earlier.
     */
    if (iter->cluster == nb_classes - 1) {
      *(pucImaRes+(d*3)) = 255;
      *(pucImaRes+(d*3)+1) = 0;
      *(pucImaRes+(d*3)+2) = 0;
    } else {
      /* Divide the sum by 4 so that no fully white pixel 
       * that is not classified as cloud will be counted
       * (usefull for auto.c) */
      unsigned int moy = (*(pucImaOrig+(d*3))    
                         + *(pucImaOrig+(d*3)+1)
                         + *(pucImaOrig+(d*3)+2))/4;

      *(pucImaRes+(d*3)) = moy;
      *(pucImaRes+(d*3) + 1) = moy;
      *(pucImaRes+(d*3) + 2) = moy;
    }
    iter += 1;
  }
    
  /* free allocated space */
  free(cl_px);
  for (int i = nb_classes - 1; i > 0 ; i--) {
    free(mass_centers[i]);
    free(newMCs[i]);
  }
  free(mass_centers);
  free(newMCs);
  free(nb_px);
}
