#ifndef _DATA_H
# define _DATA_H

struct data {
  unsigned int *px; // Pointer on 4-connexity Vpixel of the pixel
  unsigned int cluster; // The current cluster (class) assigned to the pixel
};

#endif
