#include <dirent.h>
#include <string.h>
#include <stdio.h>
#include <gtk/gtk.h>

#include "compute.h"

int main(int argc, char **argv) {
  struct dirent *dir;

  DIR *dr = opendir("../IMAGES/");

  if (dr != NULL) {
    printf("\n============== BEGIN ==============\n");
    while ((dir = readdir(dr)) != NULL) {
      if (dir->d_type == DT_REG) {
        const char *ext = strrchr(dir->d_name,'.');
        if((!ext) || (ext == dir->d_name))
          break;
        else {
          if(strcmp(ext, ".bmp"))
            continue;
          printf("\n");
          int NbCol;
          int NbLine;
          GdkPixbuf *pGdkPixbufImaOrigine;
          GdkPixbuf *pGdkPixbufImaResultat;
          char *images_path = "../IMAGES/";
          char *image_path = malloc(strlen(images_path) + strlen(dir->d_name) + 1);
          strcpy(image_path, images_path);
          strcat(image_path, dir->d_name);
          pGdkPixbufImaOrigine = gdk_pixbuf_new_from_file(image_path, NULL);
          pGdkPixbufImaResultat = gdk_pixbuf_copy(pGdkPixbufImaOrigine);
          NbCol = gdk_pixbuf_get_width(pGdkPixbufImaOrigine);
          NbLine = gdk_pixbuf_get_height(pGdkPixbufImaOrigine);
          guchar *pucImaOrigine = gdk_pixbuf_get_pixels(pGdkPixbufImaOrigine);
          guchar *pucImaResultat = gdk_pixbuf_get_pixels(pGdkPixbufImaResultat);
          ComputeImage(pucImaOrigine, NbLine, NbCol, pucImaResultat);
          int nb_pixels = 0;
          int idx = 0;
          guchar *it = pucImaResultat;
          while (idx < NbCol*NbLine*3) {
            if (*(it) == 255)
              nb_pixels++;
            it = it + 3;
            idx += 3;
          }
          printf("Image %s: %f %% of clouds", dir->d_name,
              ((float)(((float)(nb_pixels)/(float)(NbLine*NbCol)))*(float)100));
        }
      }
    }
    printf("\n\n============== END ==============\n");
  }
  return 0;
}
