#include "utils.h"

/*
 * Returns the manhattan distance between the point represented by
 * the vector V from the point represented by the vector C
 *
 * This is the distance used to compute distance between mass_centers
 * and pixels. It is massively used nowadays.
 */
float dist_manht(unsigned int *V, unsigned int* C) {
    float sum = 0;
    for (unsigned int i = 0; i < 5; i++)
        sum += abs(((int)(C[i])) - ((int)V[i]));
    return sum;
}

/*
 * Returns an integer positive if b > a, negative otherwise. It is used
 * by qsort for sorting Vpixel arrays.
 */
int compare (void const *a, void const *b)
{
    int const *pa = a;
    int const *pb = b;
    return *pb - *pa;
}
