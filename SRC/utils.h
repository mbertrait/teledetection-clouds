#ifndef _UTILS_H
# define _UTILS_H

#include <stdlib.h>
#include <math.h>
#include <gtk/gtk.h>

float dist_manht(unsigned int *V, unsigned int* C);
int compare (void const *a, void const *b);

#endif
